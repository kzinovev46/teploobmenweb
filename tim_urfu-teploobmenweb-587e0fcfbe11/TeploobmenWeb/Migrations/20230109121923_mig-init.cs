﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TeploobmenWeb.Migrations
{
    public partial class miginit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Email = table.Column<string>(type: "TEXT", nullable: false),
                    Password = table.Column<string>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Variants",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<int>(type: "INTEGER", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    InitTempMat = table.Column<double>(type: "REAL", nullable: false),
                    InitTempGas = table.Column<double>(type: "REAL", nullable: false),
                    GasSpeed = table.Column<double>(type: "REAL", nullable: false),
                    AvgHeatGas = table.Column<double>(type: "REAL", nullable: false),
                    MatConsumption = table.Column<double>(type: "REAL", nullable: false),
                    HeatMat = table.Column<double>(type: "REAL", nullable: false),
                    HeatCoeff = table.Column<double>(type: "REAL", nullable: false),
                    Diameter = table.Column<double>(type: "REAL", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Variants", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Variants");
        }
    }
}
