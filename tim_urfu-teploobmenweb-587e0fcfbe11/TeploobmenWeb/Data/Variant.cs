﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeploobmenWeb.Data
{
    public class Variant
    {
        [Key]
        public int Id { get; set; }

        public int? UserId { get; set; }

        public string Name { get; set; }

        public double InitTempMat { get; set; }
        public double InitTempGas { get; set; }
        public double GasSpeed { get; set; }
        public double AvgHeatGas { get; set; }
        public double MatConsumption { get; set; }
        public double HeatMat { get; set; }
        public double HeatCoeff { get; set; }
        public double Diameter { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime CreatedAt { get; set; }
    }
}
