﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Diagnostics;
using TeploobmenLibrary;
using TeploobmenWeb.Data;
using TeploobmenWeb.Models;

namespace TeploobmenWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly ApplicationContext _context;

        private int _userId;

        public HomeController(ILogger<HomeController> logger, ApplicationContext applicationContext)
        {
            _logger = logger;
            _context = applicationContext;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            int.TryParse(User.FindFirst("Id")?.Value, out _userId);
        }

        [HttpPost]
        public IActionResult Result(TeploobmenInput input)
        {
            // Сохранение варианта исходных данных
            if (!string.IsNullOrEmpty(input.Name))
            {
                var existVariant = _context.Variants.FirstOrDefault(x => x.Name == input.Name);

                if (existVariant != null)
                {
                    existVariant.InitTempMat = input.InitTempMat;
                    existVariant.InitTempGas = input.InitTempGas;
                    existVariant.GasSpeed = input.GasSpeed;
                    existVariant.AvgHeatGas = input.AvgHeatGas;
                    existVariant.MatConsumption = input.MatConsumption;
                    existVariant.HeatMat = input.HeatMat;
                    existVariant.HeatCoeff = input.HeatCoeff;
                    existVariant.Diameter = input.Diameter; 

                    _context.Variants.Update(existVariant);
                    _context.SaveChanges();
                }
                else
                {
                    var variant = new Variant
                    {
                        Name = input.Name,
                        InitTempMat = input.InitTempMat,
                        InitTempGas = input.InitTempGas,
                        GasSpeed = input.GasSpeed,
                        AvgHeatGas = input.AvgHeatGas,
                        MatConsumption = input.MatConsumption,
                        HeatMat = input.HeatMat,
                        HeatCoeff = input.HeatCoeff,
                        Diameter = input.Diameter, 
                        UserId = _userId,
                        CreatedAt = DateTime.Now
                    };

                    _context.Variants.Add(variant);
                    _context.SaveChanges();
                }               
            }

            // Выполнение расчета
            var lib = new TeploobmenSolver(input);
            var result = lib.Solve();

            return View(result);
        }

        [HttpGet]
        public IActionResult Index(int? variantId)
        {
            var viewModel = new HomeViewModel();

            if (variantId != null)
            {
                viewModel.Variant = _context.Variants
                    .Where(x => x.UserId == _userId || x.UserId == 0)
                    .FirstOrDefault(x => x.Id == variantId);
            }

            viewModel.Variants = _context.Variants
                .Where(x => x.UserId == _userId || x.UserId == 0)
                .ToList();

            return View(viewModel);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Remove(int? variantId)
        {
            var variant = _context.Variants
                .Where(x => x.UserId == _userId || x.UserId == 0)
                .FirstOrDefault(x => x.Id == variantId);

            if (variant != null)
            {
                _context.Variants.Remove(variant);
                _context.SaveChanges();

                TempData["message"] = $"Вариант {variant.Name} удален.";
            }
            else
            {
                TempData["message"] = $"Вариант не найден.";
            }

            return RedirectToAction(nameof(Index));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}