﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeploobmenLibrary
{
    public class TeploobmenInput
    {

        public string Name { get; set; }

        public double InitTempMat { get; set; } //Начальная температура материала, 0С
        public double InitTempGas { get; set; } //Начальная температура газа, 0С
        public double GasSpeed { get; set; } //Скорость газа на свободное сечение шахты, м/с
        public double AvgHeatGas { get; set; } // Средняя теплоемкость газа
        public double MatConsumption { get; set; } //Расход материалов кг/с
        public double HeatMat { get; set; } // Теплоемкость материалов, кДж/(кг • К)
        public double HeatCoeff { get; set; } // Объемный коэффициент теплоотдачи, Вт/(м3 • К)
        public double Diameter { get; set; } //Диаметр аппарата, м



    }
}
