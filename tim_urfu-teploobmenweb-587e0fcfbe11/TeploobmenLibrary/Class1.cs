﻿
namespace TeploobmenLibrary
{
    public class TeploobmenSolver
    {
        public double InitTempMat { get; set; }
        public double InitTempGas { get; set; }
        public double GasSpeed { get; set; }
        public double AvgHeatGas { get; set; }
        public double MatConsumption { get; set; }
        public double HeatMat { get; set; }
        public double HeatCoeff { get; set; }
        public double Diameter { get; set; }
        public TeploobmenSolver(TeploobmenInput input)
        {
            InitTempMat = input.InitTempMat;
            InitTempGas = input.InitTempGas;
            GasSpeed = input.GasSpeed;
            AvgHeatGas = input.AvgHeatGas;
            MatConsumption = input.MatConsumption;
            HeatMat = input.HeatMat;
            HeatCoeff = input.HeatCoeff;
            Diameter = input.Diameter;
        }

        public TeploobmenOutput Solve()
        {   // временные переменные
            double _RelHeight;
            double _intermediate1;
            double _intermediate2;
            double _V;
            double _RelTempGas;
            double _temp;
            double _Temp;
            double _Diff;

            double[] ys = new double[11];
            var height = 5;

            for(int i = 0; i <= 10; i=i+1)
            {
                ys[i] = Math.Round(0.1 * i * height, 3);
            }
            double m = (HeatMat * MatConsumption) /(GasSpeed * AvgHeatGas * Math.PI * Math.Pow(Diameter, 2) / 4);
            double Y0 = (HeatCoeff * height) / (GasSpeed * AvgHeatGas * 1000);

            var model = new TeploobmenOutput()
            {
                Rows = new List<TeploobmenOutputRow>()
            };

            foreach (var y in ys)
            {
                _RelHeight = (HeatCoeff * y) / (GasSpeed * AvgHeatGas) / 1000;
                _intermediate1 = 1 - Math.Exp((m - 1) * (_RelHeight/ m));
                _intermediate2 = 1 - m*Math.Exp((m - 1) * (_RelHeight / m));
                _V = _intermediate1 / (1 - m * Math.Exp((m - 1) * Y0 / m));
                _RelTempGas = _intermediate2 / (1 - m * Math.Exp((m - 1) * Y0 / m));
                _temp = InitTempMat + (InitTempGas - InitTempMat) * _V;
                _Temp = InitTempMat + (InitTempGas - InitTempMat) * _RelTempGas;
                _Diff = _temp - _Temp;
                var row = new TeploobmenOutputRow
                {
                    Y = y,
                    RelHeight = _RelHeight,
                    Intermediate1 = _intermediate1,
                    Intermediate2 = _intermediate2,
                    V = _V,
                    RelTempGas = _RelTempGas,
                    temp = _temp,
                    Temp = _Temp,
                    Diff = _Diff
                };
                model.Rows.Add(row);
            }

            return model;
        }
    }
}